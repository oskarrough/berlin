require('isomorphic-fetch')

// https://jobs.smartrecruiters.com/sr-jobs/company-lookup?q=a

const buildSearch = (search) => {
	// `https://jobs.smartrecruiters.com/sr-jobs/company-lookup?q=${search}`
	return `https://jobs.smartrecruiters.com/sr-jobs/search?keyword=${search}&limit=100`
}

const main = async () => {
	let data;
	try {
		let res = await fetch(buildSearch('berlin'))
			.then(response => response.json())
		console.log(res)
		data =  res.content
	} catch (error) {
		console.log('error fetching', error)
	}

	let uniqueCompanies = {}
	data.forEach(item => {
		uniqueCompanies[item.company.identifier] = item.company.name
	})
	console.log(uniqueCompanies)
}


main()
