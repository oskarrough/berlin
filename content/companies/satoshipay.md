+++
body = "#startup that processes #bitcoin (stellar lumens - xlm) #micropayment transactions. Develops a two-way #payment platform that will allow content providers to charge consumers a small fee to read, watch, or listen to content. #software #tech #blockchain #xlm #fintech"
created_at = "2017-07-12T13:09:59.693Z"
is_approved = true
job_board_url = "https://satoshipay.io/jobs"
latitude = 52.513757
longitude = 13.396494
slug = "satoshipay"
tags = ["startup", "bitcoin", "micropayment", "payment", "software", "tech", "blockchain", "xlm", "fintech"]
title = "SatoshiPay"
updated_at = "2019-06-16T10:36:08.540Z"

+++
