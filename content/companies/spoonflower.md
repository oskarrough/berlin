+++
body = "On-demand, digital printing company that prints custom fabric, wallpaper, and gift wrap. #print #startup #fashion"
created_at = "2019-11-12T09:24:22.991Z"
is_approved = true
job_board_url = "https://jobs.spoonflower.com/work-at-spoonflower/"
latitude = 52.4786533
longitude = 13.4500108
slug = "spoonflower"
tags = ["print", "startup", "fashion"]
title = "Spoonflower"

+++
