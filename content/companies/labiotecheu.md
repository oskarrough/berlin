+++
body = "#digital-media covering business and innovations in the European #bio-technology industry; #media #technology #europe"
created_at = "2018-02-07T19:56:27.312Z"
is_approved = true
job_board_url = "https://labiotech.eu/join-us"
latitude = 52.5548763
longitude = 13.4109338
slug = "labiotecheu"
tags = ["digital-media", "bio-technology", "media", "technology", "europe"]
title = "Labiotech.eu"
updated_at = "2019-06-16T10:36:09.730Z"

+++
