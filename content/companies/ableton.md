+++
body = "Ableton is a #music #software company that produces and distributes the production and performance program Ableton Live and a collection of related instruments and sample libraries, as well as their own #hardware controller Ableton Push"
created_at = "2017-06-28T20:19:38.198Z"
is_approved = true
job_board_url = "https://www.ableton.com/en/jobs"
latitude = 52.5296161
longitude = 13.4103097
slug = "ableton"
tags = ["music", "software", "hardware"]
title = "Ableton"
updated_at = "2019-06-16T10:36:09.626Z"

+++
