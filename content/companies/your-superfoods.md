+++
body = "We’ve created a product with the nutritional power\nof fresh produce and the convenience of supplements. #bio #food #startup #healthy"
created_at = "2018-02-08T09:11:03.908Z"
is_approved = true
job_board_url = "https://yoursuperfoods.de/pages/jobs"
latitude = 52.5352256
longitude = 13.3776897
slug = "your-superfoods"
tags = ["bio", "food", "startup", "healthy"]
title = "Your Superfoods"
updated_at = "2019-06-16T10:36:08.532Z"

+++
