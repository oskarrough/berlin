+++
job_board_url = "https://thestudenthotel.recruitee.com/"
title = "The Student Hotel"
slug = "the-student-hotel"
body = "At The Student Hotel, we are committed to creating state-of-the-art facilities and colourful experiences we believe will change the way we live, work and travel and that redefine our understanding of what makes a ‘student’"
tags = ["startup", "spaces"]
address = "Alexanderstraße 40"
postal_code = "10179"
city="berlin"
country="germany"
latitude = 52.517570
longitude = 13.417794 
created_at = "2020-04-04T15:48:08.626Z"
updated_at = "2020-04-04T15:48:08.626Z"
company_url = "https://www.thestudenthotel.com"
linkedin_url = "https://www.linkedin.com/company/the-student-hotel"
twitter_url = "https://twitter.com/TheStudentHotel"
facebook_url = "https://www.facebook.com/TheStudentHotel"
googleMapsUrl = "https://g.page/the-student-hotel-berlin"
is_approved = true
draft = false
+++
