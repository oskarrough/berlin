+++
body = "Recordsale is an online #vinyl #shop allowing to find and buy vinyl #records"
created_at = "2017-06-28T20:30:45.610Z"
is_approved = true
job_board_url = "http://www.recordsale.de/ger/php/jobs.php"
latitude = 52.5850806
longitude = 13.357539
slug = "recordsale"
tags = ["vinyl", "shop", "records"]
title = "Recordsale"
updated_at = "2019-06-16T10:36:09.728Z"

+++
