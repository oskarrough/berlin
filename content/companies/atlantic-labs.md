+++
job_board_url = "https://atlanticlabs.recruitee.com"
title = "Atlantic Labs"
slug = "atlantic-labs"
body = "We partner with founders early, providing the capital, tools and network necessary to growing their companies and accompanying them on their entrepreneurial journey."
tags = ["digital-health", "future-of-work", "machine-learning", "decentralized-networks", "mobility", "industry"]
address = "Rosenthaler Straße 13"
postal_code = "10119"
city="berlin"
country="germany"
latitude = 52.527573
longitude = 13.402867 
created_at = "2020-04-04T14:47:08.626Z"
updated_at = "2020-04-04T14:47:08.626Z"
company_url = "https://www.atlanticlabs.de"
linkedin_url = "https://www.linkedin.com/company/atlantic-labs/"
is_approved = true
draft = false
+++
