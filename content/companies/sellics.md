+++
job_board_url = "https://sellicsjobs.recruitee.com"
title = "Sellics"
slug = "sellics"
body = "We are revolutionizing eCommerce starting with the world’s biggest marketplace, Amazon. With our all-in-one solution, we help our diverse customer base of Sellers, Vendors, and Agencies achieve success on Amazon."
tags = ["startup", "ecommerce", "amazon"]
address = "Linienstraße 214"
postal_code = "10119"
city="berlin"
country="germany"
latitude = 52.528650
longitude = 13.405936
googleMapsUrl = "https://goo.gl/maps/JnkdfQDUg4TwZzi68"
company_url = "https://sellics.com"
linkedin_url = "https://www.linkedin.com/company/sellics"
twitter_url = "https://twitter.com/sellics_amz"
facebook_url = "https://www.facebook.com/SellicsEN"
instagram_url = "https://www.instagram.com/sellicsteam"
created_at = "2020-04-04T16:57:08.626Z"
updated_at = "2020-04-04T16:57:08.626Z"
is_approved = true
draft = false
+++
