+++
body = "We ourselves are musicians and music lovers – and we’re passionate about our mission to change the live music industry.Here at gigmit, our goal is to get talented artists where they belong: on stage! #music"
created_at = "2018-02-09T06:14:38.933Z"
is_approved = true
job_board_url = "https://www.gigmit.com/en/jobs"
latitude = 52.491014
longitude = 13.418579
slug = "gigmit"
tags = ["music"]
title = "Gigmit"
updated_at = "2019-06-16T10:36:09.622Z"

+++
