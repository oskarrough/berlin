+++
body = "Berlin-based operational #venture-capital that provides #capital, #network, and operational #expertise. #tech"
created_at = "2018-01-23T17:07:46.257Z"
is_approved = true
job_board_url = "https://www.project-a.com/en/career/jobs"
latitude = 52.532285
longitude = 13.384316
slug = "project-a-ventures"
tags = ["venture-capital", "capital", "network", "expertise", "tech"]
title = " Project A Ventures"
updated_at = "2019-06-16T10:36:09.631Z"

+++
