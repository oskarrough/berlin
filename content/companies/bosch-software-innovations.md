+++
body = "Internet of Things (#iot) #software company"
created_at = "2018-02-02T05:32:55.236Z"
is_approved = true
job_board_url = "https://www.bosch-si.com/de/unternehmen/karriere/stellen/angebote.html"
latitude = 52.4539579
longitude = 13.3878757
slug = "bosch-software-innovations"
tags = ["iot", "software"]
title = "Bosch Software Innovations"
updated_at = "2019-06-16T10:36:08.538Z"

+++
