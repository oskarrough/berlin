+++
body = "#fintech #venture-capita based in Berlin. #vc"
created_at = "2017-12-13T19:47:05.630Z"
is_approved = true
job_board_url = "https://www.finleap.com/careers/jobs/"
latitude = 52.5081977
longitude = 13.3298622
slug = "finleap"
tags = ["fintech", "venture-capita", "vc"]
title = "FinLeap"
updated_at = "2019-06-16T10:36:09.736Z"

+++
