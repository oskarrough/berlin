+++
job_board_url = "https://plantix.recruitee.com/"
title = "PLantix (PEAT)"
slug = "plantix-peat"
body = "Startup with the passion of bringing meaningful change and security to food production worldwide. We empower family farmers and growers to fight crop shortfalls and unnecessary pesticide use, with the help of our smart and free App Plantix. Plantix comes equipped with AI Computer Vision algorithms that detect 400 plant diseases, pests, and nutrient deficiencies and help farmers to get the help they need. Our users can be found worldwide, with a high concentration in India and Latin America."
tags = ["startup", "food", "production", "farming", "app", "ai", "computer-vision", "agriculture"]
address = "Kastanienallee 4"
postal_code = "10435"
city="berlin"
country="germany"
latitude = 52.540450
longitude = 13.410877
company_url = "https://plantix.net"
linkedin_url = ""
twitter_url = "https://twitter.com/plantixApp"
facebook_url = "https://www.facebook.com/Plantix"
instagram_url = ""
created_at = "2020-03-04T17:11:18.626Z"
updated_at = "2020-03-04T17:11:18.626Z"
is_approved = true
draft = false
+++
