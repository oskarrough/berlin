+++
body = "Social ordering app that is changing the way people order food online. #startup #delivery #food"
created_at = "2019-05-31T14:29:36.713Z"
is_approved = true
job_board_url = "https://ritual.co/careers"
slug = "ritualco"
tags = ["startup", "delivery", "food"]
title = "Ritual.co"
updated_at = "2019-06-16T10:36:09.730Z"

+++
