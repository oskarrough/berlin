+++
body = "#neuro-technology #startup"
created_at = "2018-02-09T11:12:53.750Z"
is_approved = true
job_board_url = "http://neurosphere.io/#jobs"
latitude = 52.517783
longitude = 13.4032986
slug = "neurosphere"
tags = ["neuro-technology", "startup"]
title = "Neurosphere"
updated_at = "2019-06-16T10:36:09.724Z"

+++
