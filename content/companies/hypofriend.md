+++
body = "Free online #mortgage recommendation calculator."
created_at = "2020-02-25T11:23:59.189Z"
is_approved = true
job_board_url = "https://hypofriend.de/en/careers"
latitude = 52.529405
longitude = 13.405102
slug = "hypofriend"
tags = ["mortgage"]
title = "Hypofriend"

+++
