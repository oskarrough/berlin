+++
body = "#multinational #hospitality company managing #hotels and resorts"
created_at = "2018-01-29T10:31:05.283Z"
is_approved = true
job_board_url = "https://jobs.marriott.com/jobs?stretch=10&stretchUnit=MILES&location=Berlin%2C+Germany&woe=7&page=1"
slug = "marriott-hotels"
tags = ["multinational", "hospitality", "hotels"]
title = "Marriott Hotels"
updated_at = "2019-06-16T10:36:09.627Z"

+++
