+++
body = "#academic #publishing; #science"
created_at = "2017-06-28T20:38:45.228Z"
is_approved = true
job_board_url = "http://www.springernature.com/gp/group/careers"
latitude = 52.4751227
longitude = 13.3389373
slug = "springer-nature"
tags = ["academic", "publishing", "science"]
title = "Springer Nature"
updated_at = "2019-06-16T10:36:09.728Z"

+++
