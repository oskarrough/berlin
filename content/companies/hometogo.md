+++
body = "#startup for vacation rentals; #tourism"
created_at = "2018-02-02T08:21:09.851Z"
is_approved = true
job_board_url = "https://www.hometogo.com/careers"
latitude = 52.528829
longitude = 13.3445146
slug = "hometogo"
tags = ["startup", "tourism"]
title = "HomeToGo"
updated_at = "2019-06-16T10:36:09.741Z"

+++
