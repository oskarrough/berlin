+++
body = "BMG Rights Management GmbH is an international music company focused on the management of music publishing, recording rights and music distribution.\n#music "
created_at = "2018-02-07T20:45:39.993Z"
is_approved = true
job_board_url = "https://www.bmg.com/de/career.html"
latitude = 52.5126241
longitude = 13.3882112
slug = "bmg"
tags = ["music"]
title = "BMG"
updated_at = "2019-06-16T10:36:08.537Z"

+++
