+++
body = "Hand-picked consultants. #consulting"
created_at = "2020-02-29T18:07:02.000Z"
is_approved = true
job_board_url = "https://comatch-gmbh-jobs.personio.de"
company_url = "https://www.comatch.com/"
latitude = 52.52727
longitude = 13.3419823
slug = "comatch"
tags = ["consulting"]
title = "comatch"

+++
