+++
body = "#open-source platform for workflow and decision automation that brings #business users and #software developers together"
created_at = "2018-02-01T17:27:44.382Z"
is_approved = true
job_board_url = "https://camunda.com/career/"
latitude = 52.4943557
longitude = 13.3961995
slug = "camunda"
tags = ["open-source", "business", "software"]
title = "Camunda"
updated_at = "2019-06-16T10:36:09.744Z"

+++
