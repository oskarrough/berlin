+++
job_board_url = "https://remerge.recruitee.com"
title = "Remerge"
slug = "remerge"
body = "Incremental App Retargeting"
tags = ["startup", "advertising", "retargeting"]
address = "Oranienburger Str. 27"
postal_code = "10117"
city="berlin"
country="germany"
latitude = 52.524764
longitude = 13.394987
created_at = "2020-04-04T15:48:08.626Z"
updated_at = "2020-04-04T15:48:08.626Z"
company_url = "https://remerge.io"
linkedin_url = "https://www.linkedin.com/company/remerge---app-retargeting/"
twitter_url = "https://twitter.com/remergeio"
facebook_url = "https://www.facebook.com/remerge.io"
is_approved = true
draft = false
+++
