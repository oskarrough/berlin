+++
body = "#customer-service #software company"
created_at = "2018-02-04T09:45:15.041Z"
is_approved = true
job_board_url = "https://www.zendesk.com/jobs/berlin/"
latitude = 52.524686
longitude = 13.4047183
slug = "zendesk"
tags = ["customer-service", "software"]
title = "Zendesk"
updated_at = "2019-06-16T10:36:08.534Z"

+++
