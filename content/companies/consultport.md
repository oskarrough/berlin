+++
job_board_url = "https://consultport.recruitee.com"
title = "Consultport"
slug = "consultport"
body = "Fast-growing company based in Berlin, on a mission to disrupt the way Consulting has been done up until now. We’re building the finest digital marketplace, one that brings leading digital experts and top-tier management consultants together with businesses. Our clients range from fast-growing startups to large corporations across European markets. We connect them with the highest qualified professionals to succeed with their digital strategy, digital transformation, business model development, and more."
tags = ["startup", "consulting", "marketplace", "digital", "digital-strategy", "digital-transformation"]
address = "Rheinsberger Str. 73"
postal_code = "10115"
city="berlin"
country="germany"
latitude = 52.536988
longitude = 13.395743 
created_at = "2020-04-04T15:48:08.626Z"
updated_at = "2020-04-04T15:48:08.626Z"
company_url = "https://www.consultport.com"
googleMapsUrl = "https://goo.gl/maps/rZDjMWDiAGS1ogpy6"
linkedin_url = "https://www.linkedin.com/company/consultport/"
is_approved = true
draft = false
+++
