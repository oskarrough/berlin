+++
body = "#job-board for #language (multilangual) related jobs"
created_at = "2018-06-08T13:00:33.797Z"
is_approved = true
job_board_url = "https://www.europelanguagejobs.com/jobs?keywords=berlin"
slug = "europe-language-jobs"
tags = ["job-board", "languages"]
title = "Europe Language Jobs"
updated_at = "2019-06-16T10:36:09.744Z"

+++
