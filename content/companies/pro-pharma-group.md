+++
job_board_url = "https://www.propharmagroup.com/careers"
job_board_provider = "smartrecruiters"
job_board_hostname = "ProPharmaGroup"
title = "Pro Pharma Group"
slug = "pro-pharma-group"
body = "From clinical to commercialization, and any point in between, we partner with pharmaceutical, biotechnology, and medical device clients to ensure regulatory expectations are met, business goals are achieved, and patient safety is protected."
tags = ["pharmaceutical", "biotechnology"]
address = "Siemensdamm 62"
postal_code = "13627"
city="berlin"
country="germany"
latitude = 52.533348
longitude = 13.280331
company_url = "https://www.propharmagroup.com"
linkedin_url = "https://www.linkedin.com/company/propharma-group"
crunchbase_url = "https://www.crunchbase.com/organization/propharma-group"
twitter_url = "https://twitter.com/propharmagroup"
facebook_url = ""
instagram_url = ""
created_at = "2020-07-04T18:26:18.626Z"
updated_at = "2020-07-04T18:26:18.626Z"
is_approved = true
draft = false
+++
