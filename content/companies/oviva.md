+++
body = "Digitally enabled nutritional therapy #health #startup"
created_at = "2019-10-04T21:50:41.664Z"
is_approved = true
job_board_url = "https://oviva.com/de/de/jobs/"
latitude = 52.3985388
longitude = 13.053442
slug = "oviva"
tags = ["health", "startup"]
title = "Oviva"

+++
