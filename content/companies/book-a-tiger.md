+++
body = "Online booking for cleaners #startup #cleaning"
created_at = "2018-01-29T08:39:40.441Z"
is_approved = true
job_board_url = "https://www.bookatiger.com/de-en/careers"
latitude = 52.5132373
longitude = 13.4177761
slug = "book-a-tiger"
tags = ["startup", "cleaning"]
title = "Book A Tiger"
updated_at = "2019-06-16T10:36:09.743Z"

+++
