+++
body = "Developing #games for #mobile devices; #software"
created_at = "2017-06-28T20:28:06.936Z"
is_approved = true
job_board_url = "http://www.wooga.com/jobs/"
latitude = 52.5287037
longitude = 13.4161895
slug = "wooga"
tags = ["games", "mobile", "software"]
title = "Wooga"
updated_at = "2019-06-16T10:36:09.732Z"

+++
