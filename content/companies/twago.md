+++
job_board_url = "www.twago.com/blog/jobs/"
job_board_provider = "smartrecruiters"
job_board_hostname = "Team2VentureGmbH1"
title = "Twgao"
slug = "twago"
body = "Hire freelancers to get the job done"
tags = ["freelance", "platform"]
address = "Friedrichstraße 224"
postal_code = "10969"
city="berlin"
country="germany"
latitude = 52.504221
longitude = 13.390684
company_url = "https://www.twago.com"
linkedin_url = "https://www.linkedin.com/company/team2venture-gmbh"
twitter_url = "https://twitter.com/twago_inside"
facebook_url = "https://www.facebook.com/twagocom"
created_at = "2020-07-04T18:26:18.626Z"
updated_at = "2020-07-04T18:26:18.626Z"
is_approved = true
draft = false
+++
