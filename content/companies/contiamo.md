+++
body = "#dataScience platform. #startup"
created_at = "2018-03-14T13:36:31.845Z"
is_approved = true
job_board_url = "https://contiamo.com/careers/"
latitude = 52.5242479
longitude = 13.4058121
slug = "contiamo"
tags = ["dataScience", "startup"]
title = "Contiamo"
updated_at = "2019-06-16T10:36:09.631Z"

+++
