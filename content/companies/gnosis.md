+++
body = "We believe in a redistributed future. Gnosis builds new #market mechanisms to make this future possible. Through blockchain-based, decentralized platforms, we enable the redistribution of resources—from assets to incentives, and information to ideas. To learn more, read about our mission, delve into our blog, or check out our latest research. #blockchain #decentralization #information #startup"
created_at = "2020-02-18T15:48:55.981Z"
is_approved = true
job_board_url = "https://gnosis.io/team"
latitude = 52.500339730516956
longitude = 13.436279296875002
slug = "gnosis"
tags = ["market", "blockchain", "decentralization", "information", "startup"]
title = "Gnosis"

+++
