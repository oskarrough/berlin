+++
body = "ResearchGate is a #social #networking site for #scientists and #researchers to share papers, ask and answer questions, and find collaborators"
created_at = "2017-06-28T20:27:58.103Z"
is_approved = true
job_board_url = "http://www.researchgate.net/careers"
latitude = 52.530219
longitude = 13.383646
slug = "researchgate"
tags = ["social", "networking", "scientists", "researchers"]
title = "ResearchGate"
updated_at = "2019-06-16T10:36:09.730Z"

+++
