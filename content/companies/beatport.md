+++
body = "Online electronic #music store"
created_at = "2017-06-28T20:19:59.841Z"
is_approved = true
job_board_url = "http://about.beatport.com/careers"
latitude = 52.5028033
longitude = 13.4408174
slug = "beatport"
tags = ["music"]
title = "Beatport"
updated_at = "2019-06-16T10:36:08.531Z"

+++
