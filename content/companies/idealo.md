
+++
job_board_url = "https://idealocareer.recruitee.com"
title = "Idealo"
body = "Price comparison service, allows users to compare prices on a range of products from hundreds of shops."
tags = ["startup", "price-comparison"]
address = "Ritterstraße 11"
postal_code = "10969"
city="berlin"
country="germany"
company_url = "https://www.idealo.de"
slug = "idealo"
latitude = 52.501372
longitude = 13.411300
linkedin_url = "https://www.linkedin.com/company/idealo-internet-gmbh"
twitter_url = "https://twitter.com/idealo_de"
instagram_url = "https://www.instagram.com/idealo"
created_at = "2020-08-04T19:35:18.626Z"
updated_at = "2020-08-04T19:35:18.626Z"
is_approved = true
draft = false
+++
