+++
body = "Ace & Tate is an #online #glasses #shop enabling you to find and purchase several glasses frames"
created_at = "2017-06-28T20:34:24.771Z"
is_approved = true
job_board_url = "https://www.aceandtate.de/jobs"
latitude = 52.5141906
longitude = 13.3676806
slug = "ace-tate"
tags = ["online", "glasses", "shop"]
title = "Ace & Tate"
updated_at = "2019-06-16T10:36:08.533Z"

+++
