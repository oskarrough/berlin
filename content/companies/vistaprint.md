+++
body = "#online #printing company offering a large range of custom items"
created_at = "2017-06-28T20:34:45.787Z"
is_approved = true
job_board_url = "http://careers.vistaprint.com/search?searchCulture=en&searchText=&searchCareerTracks=&searchLocations=Berlin"
latitude = 52.5160189
longitude = 13.3280522
slug = "vistaprint"
tags = ["online", "printing"]
title = "Vistaprint"
updated_at = "2019-06-16T10:36:08.536Z"

+++
