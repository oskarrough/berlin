+++
body = "online #tv #platform for IP-based transmission of #television #channels and #video on demand content to a variety of devices; #tech "
created_at = "2017-06-28T20:35:48.971Z"
is_approved = true
job_board_url = "http://zattoo.com/company/en/jobs/"
latitude = 52.47399
longitude = 13.4574199
slug = "zattoo"
tags = ["tv", "platform", "television", "channels", "video", "tech"]
title = "Zattoo"
updated_at = "2019-06-16T10:36:08.539Z"

+++
