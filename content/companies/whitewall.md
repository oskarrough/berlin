+++
body = "#photo prints, mounting & framing solutions for professional #photographers; #printing"
created_at = "2017-06-28T20:35:26.814Z"
is_approved = true
job_board_url = "https://de.whitewall.com/jobs"
latitude = 52.523768
longitude = 13.4014659
slug = "whitewall"
tags = ["photo", "photographers", "printing"]
title = "Whitewall"
updated_at = "2019-06-16T10:36:08.533Z"

+++
