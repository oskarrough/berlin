+++
body = "AUTO1 Group is one of Europe’s leading #car #trading #platform. It enables consumers as well as dealers to trade seamlessly throughout Europe"
created_at = "2018-02-01T20:29:56.549Z"
is_approved = true
job_board_url = "https://www.auto1.com/de/jobs"
latitude = 52.4891729
longitude = 13.3976753
slug = "auto1"
tags = ["car", "trading", "platform"]
title = "Auto1"
updated_at = "2019-06-16T10:36:09.624Z"

+++
