+++
body = "Private #clinic group"
created_at = "2018-02-09T18:59:36.015Z"
is_approved = true
job_board_url = "https://www.sana.de/karriere/jobangebote/?tx_asjobboerse_pi3%5Bbundesland%5D=3&tx_asjobboerse_pi1%5Bpage%5D=1&cHash=92f1534839040be64d311ed86083fbc4"
latitude = 52.5032066
longitude = 13.4732158
slug = "sana-kliniken"
tags = ["clinic", "medical", "health"]
title = "Sana Kliniken"
updated_at = "2019-06-16T10:36:09.639Z"

+++
