+++
body = "Agency that provides hourly care for the #elderly and those in need of #care"
created_at = "2018-02-06T06:24:13.590Z"
is_approved = true
job_board_url = "https://careship-jobs.personio.de/"
latitude = 52.5321601
longitude = 13.4270243
slug = "careship"
tags = ["elderly", "care"]
title = "Careship"
updated_at = "2019-06-16T10:36:09.735Z"

+++
