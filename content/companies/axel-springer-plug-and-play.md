+++
body = "Berlin-based #pre-seed #startup #accelerator ; #finance"
created_at = "2017-07-12T06:41:56.909Z"
is_approved = true
job_board_url = "http://www.axelspringerplugandplay.com/jobs/"
latitude = 52.5055927
longitude = 13.3948897
slug = "axel-springer-plug-and-play"
tags = ["pre-seed", "startup", "accelerator", "finance"]
title = "Axel Springer Plug And Play"
updated_at = "2019-06-16T10:36:09.728Z"

+++
