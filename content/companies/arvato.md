+++
body = "Arvato is a global services company . Its services include #customerSupport, #informationTechnology, #logistics, and #finance."
created_at = "2018-02-22T09:56:41.837Z"
is_approved = true
job_board_url = "https://www.arvato.com/en/career/job-offers.html"
latitude = 52.5338172
longitude = 13.2679368
slug = "arvato"
tags = ["customerSupport", "informationTechnology", "logistics", "finance"]
title = "Arvato"
updated_at = "2019-06-16T10:36:09.626Z"

+++
