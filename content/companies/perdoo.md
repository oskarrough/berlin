+++
job_board_url = "https://perdoo.recruitee.com/"
title = "Perdoo"
slug = "perdoo"
body = "Perdoo is a simple yet powerful goal management platform used by thousands of employees in hundreds of companies across the world."
tags = ["startup", "goal-mangement", "platform"]
address = "Straße der Pariser Kommune, 8 "
postal_code = "10243"
city="berlin"
country="germany"
latitude =  52.513058
longitude = 13.439078
created_at = "2020-04-04T13:58:09.626Z"
updated_at = "2020-04-04T13:59:09.626Z"
is_approved = true
draft = false
+++
