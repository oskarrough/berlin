+++
body = "RV rental platform #startup"
created_at = "2018-01-27T10:52:31.835Z"
is_approved = true
job_board_url = "https://campanda-gmbh.workable.com"
latitude = 52.501604
longitude = 13.3255743
slug = "campanda"
tags = ["startup"]
title = "Campanda"
updated_at = "2019-06-16T10:36:09.642Z"

+++
