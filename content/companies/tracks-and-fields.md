+++
body = "Tracks and Fields helps you to find the best #music for your projects in #advertising, #film and #interactive. All according to your music briefing, time constraints and budge"
created_at = "2017-06-28T20:31:23.024Z"
is_approved = true
job_board_url = "https://www.tracksandfields.com/blog/category/jobs/"
latitude = 52.5361833
longitude = 13.4024721
slug = "tracks-and-fields"
tags = ["music", "advertising", "film", "interactive"]
title = "Tracks and Fields"
updated_at = "2019-06-16T10:36:08.531Z"

+++
