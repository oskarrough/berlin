+++
body = "REWE is a German diversified #retail and #tourism #co-operative group. The basis of the co-operative trade group consists of a network of independent #retailers such as #supermarkets and convenience stores"
created_at = "2017-06-28T20:39:39.235Z"
is_approved = true
job_board_url = "https://karriere.rewe-group.com/search/?q=&locationsearch=berlin"
slug = "rewe-group"
tags = ["retail", "tourism", "co-operative", "retailers", "supermarkets"]
title = "REWE Group"
updated_at = "2019-06-16T10:36:09.735Z"

+++
