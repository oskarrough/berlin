+++
body = "Medigo is an #international #network of #medical experts"
created_at = "2017-06-28T20:33:03.513Z"
is_approved = true
job_board_url = "https://www.medigo.com/en/careers"
latitude = 52.5274944
longitude = 13.40285
slug = "medigo"
tags = ["international", "network", "medical"]
title = "Medigo"
updated_at = "2019-06-16T10:36:09.626Z"

+++
