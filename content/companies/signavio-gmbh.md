+++
body = "Vendor of a Business Process Management #software"
created_at = "2018-02-05T09:27:36.519Z"
is_approved = true
job_board_url = "https://www.signavio.com/jobs/"
latitude = 52.5054978
longitude = 13.3394667
slug = "signavio-gmbh"
tags = ["software"]
title = "Signavio GmbH"
updated_at = "2019-06-16T10:36:08.537Z"

+++
