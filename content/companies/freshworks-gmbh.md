+++
body = "Customer engagement #software #startup"
created_at = "2019-05-10T09:21:13.523Z"
is_approved = true
job_board_url = "https://careers.freshworks.com/jobs/search?utf8=%E2%9C%93&%5Bquery%5D=&%5Bbranch_id%5D=16&commit=Go"
latitude = 52.5096226
longitude = 13.407043
slug = "freshworks-gmbh"
tags = ["software", "startup"]
title = "Freshworks GmbH"
updated_at = "2019-06-16T10:36:09.735Z"

+++
