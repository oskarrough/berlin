+++
body = "#real-estate #software #startup"
created_at = "2018-02-01T21:55:54.282Z"
is_approved = true
job_board_url = "https://www.architrave.de/ueber-uns/karriere/"
latitude = 52.4923632
longitude = 13.4512515
slug = "architrave-gmbh"
tags = ["real-estate", "software", "startup"]
title = "Architrave GmbH"
updated_at = "2019-06-16T10:36:08.536Z"

+++
