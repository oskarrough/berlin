+++
body = "Resident Advisor (RA) is an online #music #magazine and #community #platform dedicated to showcasing #electronic-music, artists and events across the globe"
created_at = "2017-06-28T20:36:07.308Z"
is_approved = true
job_board_url = "https://www.residentadvisor.net/about-work.aspx"
slug = "resident-advisor"
tags = ["music", "magazine", "community", "platform", "electronic-music"]
title = "Resident Advisor"
updated_at = "2019-06-16T10:36:08.538Z"

+++
