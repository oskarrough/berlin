+++
body = "iversity.org is an online #learning platform for higher #education and professional development #courses provided by experts from all over Europe"
created_at = "2017-06-28T20:30:06.351Z"
is_approved = true
job_board_url = "https://iversity.org/en/pages/jobs"
latitude = 52.5648306
longitude = 13.4398136
slug = "iversity"
tags = ["learning", "education", "courses"]
title = "Iversity"
updated_at = "2019-06-16T10:36:08.519Z"

+++
