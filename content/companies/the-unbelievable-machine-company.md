+++
body = "#software and #it provider for #big-data #cloud services and hosting; #consulting"
created_at = "2018-02-03T17:51:52.547Z"
is_approved = true
job_board_url = "https://unbelievablemachine.bamboohr.co.uk/jobs/"
latitude = 52.5038104
longitude = 13.3244308
slug = "the-unbelievable-machine-company"
tags = ["software", "it", "big-data", "cloud", "consulting"]
title = "The unbelievable Machine Company"
updated_at = "2019-06-16T10:36:09.626Z"

+++
