+++
body = "Stayfriends.de is a social network service created in 2002. The #social media #website helps members find, #connect and keep in touch with #friends from #kindergarten, primary #school, high school in Germany. Stayfriends.de had more than 14 million members in Germany in 2014"
created_at = "2017-06-28T20:26:19.850Z"
is_approved = true
job_board_url = "https://www.stayfriends.com/careers"
latitude = 52.5171398
longitude = 13.3272506
slug = "stayfriends"
tags = ["social", "website", "connect", "friends", "kindergarten", "school"]
title = "StayFriends"
updated_at = "2019-06-16T10:36:08.538Z"

+++
