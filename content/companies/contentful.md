+++
body = "#startup #tech"
created_at = "2017-06-28T20:23:03.108Z"
is_approved = true
job_board_url = "https://www.contentful.com/careers/"
latitude = 52.5020754
longitude = 13.4111829
slug = "contentful"
tags = ["startup", "tech"]
title = "Contentful"
updated_at = "2019-06-16T10:36:09.731Z"

+++
