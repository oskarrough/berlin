+++
body = "Wayfair is an American #ecommerce company that sells #home goods"
created_at = "2018-02-01T14:32:37.450Z"
is_approved = true
job_board_url = "https://jobs.wayfaircareers.com/jobs?location=berlin&stretch=10&stretchUnit=MILES&page=1"
latitude = 52.5126002
longitude = 13.4152366
slug = "wayfair"
tags = ["ecommerce", "home"]
title = "Wayfair "
updated_at = "2019-06-16T10:36:09.639Z"

+++
