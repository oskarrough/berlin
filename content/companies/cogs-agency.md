+++
body = "#recruiting #agency handpicking candidates for businesses. #job-board #hr"
created_at = "2018-01-27T14:56:07.489Z"
is_approved = true
job_board_url = "http://cogsagency.com/jobs/?discipline=&location=de_DE&s="
latitude = 52.5134128
longitude = 13.3969146
slug = "cogs-agency"
tags = ["recruiting", "agency", "job-board", "hr"]
title = "Cogs Agency"
updated_at = "2019-06-16T10:36:08.535Z"

+++
