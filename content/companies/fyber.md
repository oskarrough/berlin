+++
body = "Fyber is a #technology company, developing a #monetization #platform for #mobile #publishers"
created_at = "2017-06-28T20:32:32.069Z"
is_approved = true
job_board_url = "http://www.fyber.com/careers.html"
latitude = 52.5111162
longitude = 13.4045226
slug = "fyber"
tags = ["technology", "monetization", "platform", "mobile", "publishers"]
title = "Fyber"
updated_at = "2019-06-16T10:36:09.638Z"

+++
