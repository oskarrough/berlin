+++
body = "Vimcar has quickly grown to be a surprising success story within the multi-billion-dollar connected car industry. The startup’s flagship product, a mileage tracking application, has already helped over 5.000 companies to save thousands in taxes. But even bigger plans are ahead: Vimcar’s mission is to digitise and automate every aspect of fleet management for small to medium-size businesses, which in many cases don’t have a human fleet manager of their own. #auto #car #startup"
created_at = "2018-02-09T06:49:26.337Z"
is_approved = true
job_board_url = "https://vimcar.de/career/jobs"
latitude = 52.4999017
longitude = 13.4276711
slug = "vimcar"
tags = ["auto", "car", "startup"]
title = "Vimcar"
updated_at = "2019-06-16T10:36:09.783Z"

+++
