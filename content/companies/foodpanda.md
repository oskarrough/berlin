+++
body = "#food #delivery service from a curated choice of local #restaurants; #startup"
created_at = "2017-06-28T20:27:14.077Z"
is_approved = true
job_board_url = "https://www.foodpanda.com/careers/listing/"
latitude = 52.524788
longitude = 13.3931115
slug = "foodpanda"
tags = ["food", "delivery", "restaurants", "startup"]
title = "Foodpanda"
updated_at = "2019-06-16T10:36:09.744Z"

+++
