+++
body = "#fintech #startup"
created_at = "2018-03-01T11:00:43.538Z"
is_approved = true
job_board_url = "https://www.spotcap.com/our-team-and-careers/"
latitude = 52.5007812
longitude = 13.4498679
slug = "spotcap-global-services"
tags = ["fintech", "startup"]
title = "Spotcap Global Services"
updated_at = "2019-06-16T10:36:09.731Z"

+++
