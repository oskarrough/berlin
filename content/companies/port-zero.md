+++
body = "#software development and #it-security #agency; #it"
created_at = "2018-11-20T15:32:15.590Z"
is_approved = true
job_board_url = "https://port-zero.com/jobs"
latitude = 52.49221
longitude = 13.43479
slug = "port-zero"
tags = ["software", "it-security", "agency", "it"]
title = "Port Zero"
updated_at = "2019-06-16T10:36:08.537Z"

+++
