+++
body = "#pharmaceutical #chemical #multinational"
created_at = "2019-02-04T12:51:55.546Z"
is_approved = true
job_board_url = "https://www.karriere.bayer.de/en/working-at-bayer/locations/berlin/jobs/"
latitude = 52.5423013
longitude = 13.3679809
slug = "bayer"
tags = ["pharmaceutical", "chemical", "multinational"]
title = "Bayer"
updated_at = "2019-06-16T10:36:09.750Z"

+++
