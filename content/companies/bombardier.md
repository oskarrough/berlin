+++
body = "World's leading manufacturer of both planes and trains. Everywhere people travel by land and in the air, a Bombardier product is ready to transport them. #train #aviation #engineering"
created_at = "2018-02-09T14:03:42.416Z"
is_approved = true
job_board_url = "https://jobs.bombardier.com/go/Jobs-in-Germany/325728/?q=&q2=&locationsearch=&title=&location=berlin&date="
latitude = 52.4929142
longitude = 13.3448715
slug = "bombardier"
tags = ["train", "aviation", "engineering"]
title = "Bombardier"
updated_at = "2019-06-16T10:36:09.730Z"

+++
