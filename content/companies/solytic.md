+++
body = "#startup #solar #energy-transformation"
created_at = "2018-06-07T12:00:02.629Z"
is_approved = true
job_board_url = "https://solytic-jobs.personio.de/"
latitude = 52.530681
longitude = 13.382398
slug = "solytic"
tags = ["startup", "solar", "energy-transformation"]
title = "SOLYTIC"
updated_at = "2019-06-16T10:36:08.539Z"

+++
