+++
body = "Rocket Internet #incubates and #invests in #Internet #companies with proven #business models"
created_at = "2017-06-28T20:24:09.883Z"
is_approved = true
job_board_url = "https://www.rocket-internet.com/careers"
latitude = 52.5065125
longitude = 13.3932297
slug = "rocket-internet"
tags = ["incubates", "invests", "Internet", "companies", "business"]
title = "Rocket Internet"
updated_at = "2019-06-16T10:36:08.540Z"

+++
