+++
body = "We are a dynamic startup from Berlin with the aim of making healthy nutrition available to everyone. #health #nutrition #food"
created_at = "2018-02-09T06:53:04.451Z"
is_approved = true
job_board_url = "https://naturalmojo.workable.com/"
latitude = 52.515117
longitude = 13.3895422
slug = "natural-mojo"
tags = ["health", "nutrition", "food"]
title = "Natural Mojo"
updated_at = "2019-06-16T10:36:09.623Z"

+++
