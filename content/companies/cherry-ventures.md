+++
job_board_url = "https://cherryventures.recruitee.com/"
title = "Cherry Ventures"
slug = "cherry-ventures"
body = "Seed and early stage fund, founded by a team of entrepreneurs with substantial experience in building fast-scaling technology companies. It is based in Berlin but invests across Europe at the seed stage. Its goal is to add value by supporting its venture teams in building successful and sustainable businesses."
tags = ["investment", "fund", "startup", "seed-stage", "early-stage"]
address = "Linienstraße, 165"
postal_code = "10115"
city="berlin"
country="germany"
latitude = 52.528529
longitude = 13.396951
created_at = "2020-04-04T13:39:09.626Z"
updated_at = "2020-04-04T13:39:09.626Z"
is_approved = true
+++
