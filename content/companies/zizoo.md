+++
body = "#boat rental platform. #startup #travel"
created_at = "2018-02-26T08:32:04.905Z"
is_approved = true
job_board_url = "https://www.zizoo.com/en/careers"
latitude = 52.4965949
longitude = 13.4313856
slug = "zizoo"
tags = ["boat", "startup", "travel"]
title = "Zizoo"
updated_at = "2019-06-16T10:36:08.530Z"

+++
