+++
job_board_url = "https://ynd1.recruitee.com"
slug = "ynd"
title = "YND"
body = "Digital Product Agency and Startup Studio. We work in FinTech, Wearables, Virtual Reality and Machine Intelligence. We help management teams at big companies, such as airlines, banks and telecom providers, to be innovative like startups."
tags = ["consulting", "agency", "digital-product", "startup-studio", "fintech", "wearables", "virtual-reality", "machine-intelligence", "innovation", "startup"]
address = "Monbijouplatz 5"
postal_code = "10178"
city="berlin"
country="germany"
latitude = 52.523263
longitude = 13.399036 
company_url = "https://ynd.co"
linkedin_url = "https://www.linkedin.com/company/ynd"
twitter_url = "https://twitter.com/yndconsult"
facebook_url = ""
instagram_url = ""
created_at = "2020-04-04T17:11:08.626Z"
updated_at = "2020-04-04T17:11:08.626Z"
is_approved = true
draft = false
+++
