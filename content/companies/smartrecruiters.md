+++
job_board_url = "https://jobs.smartrecruiters.com/smartrecruiters"
job_board_provider = "smartrecruiters"
job_board_hostname = "smartrecruiters"
title = "SmartRecruiters"
slug = "smartrecruiters"
body = "SmartRecruiters has a simple goal: To make hiring easy. To help businesses get the talent they need to succeed. And to help people find a job they love."
tags = ["startup", "hiring", "recruiting", "human-ressources", "job"]
address = "Winsstraße 62/63"
postal_code = "10405"
city="berlin"
country="germany"
latitude = 52.533475
longitude = 13.424052
company_url = "https://smartrecruiters.com"
linkedin_url = "https://www.linkedin.com/company/smartrecruiters"
twitter_url = "https://twitter.com/Smartrecruiters"
facebook_url = "https://twitter.com/Smartrecruiters"
instagram_url = "https://instagram.com/smartrecruiters"
created_at = "2020-07-04T16:30:18.626Z"
updated_at = "2020-07-04T16:30:18.626Z"
is_approved = true
draft = false
+++
