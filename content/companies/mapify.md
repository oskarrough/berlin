+++
body = "#startup #travel #tourism"
created_at = "2019-02-26T19:55:25.805Z"
is_approved = true
job_board_url = "https://info.mapify.travel/jobs"
latitude = 52.553
longitude = 13.40549
slug = "mapify"
tags = ["startup", "travel", "tourism"]
title = "Mapify"
updated_at = "2019-06-16T10:36:09.744Z"

+++
