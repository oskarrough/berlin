+++
body = "TikTok is the destination for short-form mobile videos. Our mission is to capture and present the worldwide creativity, knowledge, and precious life moments, directly from the mobile phone. #startup #video"
created_at = "2020-01-02T14:41:43.000Z"
is_approved = true
job_board_url = "https://careers.tiktok.com/position?keywords=&location=CT_6"
slug = "tiktok"
tags = ["startup", "video"]
title = "tiktok"

+++
