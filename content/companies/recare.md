+++
job_board_url = "https://recaregmbh.recruitee.com"
title = "Recare"
slug = "recare"
body = "Recare is a web-based, digital platform that facilitates an effective patient transfer from hospital to care providers. Our smart search tool sends targeted requests to potential providers and enables real-time, secure communication between both sides. The process is faster and more transparent for all - hospital staff, care providers, and patients."
tags = ["startup", "health", "hospital"]
address = "Alt-Moabit 103"
postal_code = "10559"
city="berlin"
country="germany"
latitude = 52.524779
longitude = 13.347322
created_at = "2020-04-04T15:48:08.626Z"
updated_at = "2020-04-04T15:48:08.626Z"
company_url = "https://www.recaresolutions.com"
linkedin_url = "https://www.linkedin.com/company/recare"
twitter_url = ""
is_approved = true
draft = false
+++
