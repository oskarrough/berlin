+++
job_board_url = "https://careers.eurofins.com"
job_board_provider = "smartrecruiters"
job_board_hostname = "Eurofins"
title = "Eurofins"
slug = "eurofins"
body = "World leader in food, environment, pharma product testing & agroscience CRO services"
tags = ["industry", "pharmaceutical", "testing", "food", "environment", "agroscience"]
address = "Rudower Chaussee 29"
postal_code = "12489"
city="berlin"
country="germany"
latitude = 52.428835 
longitude = 13.528514 
company_url = "https://www.eurofins.de"
linkedin_url = ""
twitter_url = ""
facebook_url = ""
instagram_url = ""
created_at = "2020-07-04T16:30:18.626Z"
updated_at = "2020-07-04T16:30:18.626Z"
is_approved = true
draft = false
+++
