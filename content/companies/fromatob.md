+++
body = "multimodal #travel planner and online #booking platform for destinations in all #europe; #startup"
created_at = "2017-06-28T20:37:17.655Z"
is_approved = true
job_board_url = "https://fromatob.workable.com/"
latitude = 52.5235909
longitude = 13.4049052
slug = "fromatob"
tags = ["travel", "booking", "europe", "startup"]
title = "FromAtoB"
updated_at = "2019-06-16T10:36:08.535Z"

+++
