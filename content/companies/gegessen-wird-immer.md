+++
body = "#delivery of fresh #organic #fruit and #vegetables, snacks, #groceries. #food"
created_at = "2018-02-02T10:42:46.396Z"
is_approved = true
job_board_url = "https://gegessenwirdimmer.de/jobs/"
latitude = 52.5459574
longitude = 13.3452202
slug = "gegessen-wird-immer"
tags = ["delivery", "organic", "fruit", "vegetables", "groceries", "food"]
title = "Gegessen Wird Immer"
updated_at = "2019-06-16T10:36:09.622Z"

+++
