+++
body = "#e-commerce #health company selling natural dietary supplements, organic teas, and an assortment of other superfoods and related products. #food"
created_at = "2019-02-18T07:12:17.483Z"
is_approved = true
job_board_url = "https://www.sunday.de/jobs/"
latitude = 52.5021
longitude = 13.36613
slug = "sunday-natural"
tags = ["e-commerce", "health", "food"]
title = "Sunday Natural"
updated_at = "2019-06-16T10:36:08.531Z"

+++
