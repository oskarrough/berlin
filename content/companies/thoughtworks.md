+++
body = "#tech company providing #agile #software design and delivery, as well as #consulting services. #multinational"
created_at = "2018-01-23T17:01:11.614Z"
is_approved = true
job_board_url = "https://www.thoughtworks.com/careers/browse-jobs"
latitude = 52.5075292
longitude = 13.3913559
slug = "thoughtworks"
tags = ["tech", "agile", "software", "consulting", "multinational"]
title = "ThoughtWorks"
updated_at = "2019-06-16T10:36:08.529Z"

+++
