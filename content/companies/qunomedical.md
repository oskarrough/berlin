+++
body = "Find the right doctors at home or abroad; #health #doctor #startup"
created_at = "2019-05-29T12:05:21.437Z"
is_approved = true
job_board_url = "https://www.qunomedical.com/en/careers"
latitude = 52.5282605
longitude = 13.3863227
slug = "qunomedical"
tags = ["health", "doctor", "startup"]
title = "Qunomedical"
updated_at = "2019-06-16T10:36:08.537Z"

+++
