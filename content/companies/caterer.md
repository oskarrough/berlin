+++
body = "#job-board for jobs in the #hospitality sector"
created_at = "2018-02-01T13:24:22.604Z"
is_approved = true
job_board_url = "https://www.caterer.com/jobs/in-berlin?radius=5"
slug = "caterer"
tags = ["job-board", "hospitality"]
title = "Caterer"
updated_at = "2019-06-16T10:36:09.640Z"

+++
