+++
job_board_url = "https://authenteq.recruitee.com"
title = "Authenteq"
slug = "authenteq"
body = "Electronic ID verification. Authenteq provides an Omni-Channel identity verification and KYC solution that allows your customers to verify their identity through any channel without compromising their privacy."
tags = ["startup", "spaces", "authentication", "identity"]
address = " Bernauer str 49"
postal_code = "10435"
city="berlin"
country="germany"
latitude = 52.540293
longitude = 13.403820
created_at = "2020-04-04T14:47:08.626Z"
updated_at = "2020-04-04T14:47:08.626Z"
company_url = "https://authenteq.com"
googleMapsUrl = "https://goo.gl/maps/pGQ1rK1SYFqNgm2G8"
is_approved = true
draft = false
+++
