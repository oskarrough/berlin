+++
body = "Amazon is an Online #shopping #platform presenting the earth's biggest #selection of books, magazines, music, DVDs, videos, electronics, computers, software, apparel & accessories, shoes, ..."
created_at = "2017-06-28T20:26:29.064Z"
is_approved = true
job_board_url = "http://www.amazon.jobs/location/berlin-germany#jobresults"
slug = "amazon"
tags = ["shopping", "platform", "selection"]
title = "Amazon"
updated_at = "2019-06-16T10:36:09.730Z"

+++
