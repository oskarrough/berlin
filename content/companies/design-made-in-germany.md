+++
body = "#job-board for #creative #design and #web professionals in Germany"
created_at = "2017-06-28T20:22:33.094Z"
is_approved = true
job_board_url = "http://www.designmadeingermany.de/jobs/"
slug = "design-made-in-germany"
tags = ["job-board", "creative", "design", "web"]
title = "Design Made In Germany"
updated_at = "2019-06-16T10:36:08.541Z"

+++
