+++
body = "#multinational #technology company that specializes in internet related services and products"
created_at = "2017-06-28T20:25:02.894Z"
is_approved = true
job_board_url = "https://www.google.com/about/careers/search#t=sq&q=j&jl=Berlin,Germany"
latitude = 52.523225
longitude = 13.3922897
slug = "google"
tags = ["multinational", "technology"]
title = "Google"
updated_at = "2019-06-16T10:36:09.624Z"

+++
