+++
body = "#job-board for #app-based-job (can be done through an app?) #apps"
created_at = "2018-02-05T13:04:41.879Z"
is_approved = true
job_board_url = "https://www.appjobs.com/berlin"
slug = "appjobs"
tags = ["job-board", "app-based-job", "apps"]
title = "Appjobs"
updated_at = "2019-06-16T10:36:09.631Z"

+++
