+++
job_board_url = "https://www.personal-excellence.de/en/jobs"
job_board_provider = "smartrecruiters"
job_board_hostname = "Frau2"
title = "Personal Excellence"
slug = "personal-excellence"
body = "HR consultancy agency"
tags = ["human-ressources", "consulting"]
address = "Bamberger Str. 8"
postal_code = "10777"
city="berlin"
country="germany"
latitude = 52.495612
longitude = 13.336943
company_url = "https://www.personal-excellence.de"
linkedin_url = ""
crunchbase_url = ""
twitter_url = ""
facebook_url = ""
instagram_url = ""
created_at = "2020-07-04T22:27:18.626Z"
updated_at = "2020-07-04T22:27:18.626Z"
is_approved = true
draft = false
+++
