+++
job_board_url = "https://billfrontgmbh.recruitee.com"
title = "BillFront"
slug = "billfront"
body = "Providing digital media companies with an easy to access & scalable working capital solution. "
tags = ["digitalmedia", "capital", "investment"]
address = "Krausenstraße 8"
postal_code = "10117"
city="berlin"
country="germany"
latitude = 52.509409
longitude = 13.390882 
created_at = "2020-04-04T14:47:08.626Z"
updated_at = "2020-04-04T14:47:08.626Z"
company_url = "https://billfront.com"
googleMapsUrl = "https://goo.gl/maps/ToVxdJrY23xAs9wT9"
is_approved = true
draft = false
+++
