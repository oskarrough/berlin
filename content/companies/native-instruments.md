+++
body = "#music #software and #hardware for computer-based #audio production and DJing"
created_at = "2017-06-28T20:20:17.715Z"
is_approved = true
job_board_url = "http://www.native-instruments.com/en/career-center/"
latitude = 52.4991645
longitude = 13.4477158
slug = "native-instruments"
tags = ["music", "software", "hardware", "audio"]
title = "Native instruments"
updated_at = "2019-06-16T10:36:09.639Z"

+++
