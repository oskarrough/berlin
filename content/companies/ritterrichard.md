+++
body = "\"\"#catering #agency doing #handmade kitchen creative #gastronomy; #hospitality #restaurant #kitchen #cuisine #events\"\""
created_at = "2018-02-02T10:41:42.829Z"
is_approved = true
job_board_url = "http://ritterrichard.de/jobs/"
latitude = 52.5501352
longitude = 13.3738589
slug = "ritterrichard"
tags = ["catering", "agency", "handmade", "gastronomy", "hospitality", "restaurant", "kitchen", "cuisine", "events"]
title = "RitterRichard"
updated_at = "2019-06-16T10:36:09.635Z"

+++
