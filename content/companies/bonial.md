+++
body = "Bonial offers #marketing solutions to connect shoppers with their favorite #brands and #retailers"
created_at = "2017-06-28T20:39:55.422Z"
is_approved = true
job_board_url = "http://www.bonial.com/careers/"
latitude = 52.5413933
longitude = 13.3833755
slug = "bonial"
tags = ["marketing", "brands", "retailers"]
title = "Bonial"
updated_at = "2019-06-16T10:36:09.737Z"

+++
