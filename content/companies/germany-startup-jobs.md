+++
body = "Germany Startup Jobs is a #job-board helping people to find right English and German speaking jobs within #startups"
created_at = "2017-06-28T20:36:16.598Z"
is_approved = true
job_board_url = "http://www.germanystartupjobs.com/browse-jobs/?keywords=&location=berlin"
slug = "germany-startup-jobs"
tags = ["job-board", "startups"]
title = "Germany Startup Jobs"
updated_at = "2019-06-16T10:36:09.745Z"

+++
