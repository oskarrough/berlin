+++
body = "Zipjet is Europe's leading on-demand laundry & dry cleaning service, operating in London, Paris and Berlin. With high quality cleaning and turnaround in as little as 24 hours, we take the hassle out of laundry. Simple. #startup"
created_at = "2018-02-09T06:57:19.281Z"
is_approved = true
job_board_url = "https://zipjet.bamboohr.co.uk/jobs/"
latitude = 52.5517129
longitude = 13.4358031
slug = "zipjet"
tags = ["startup"]
title = "ZipJet"
updated_at = "2019-06-16T10:36:09.741Z"

+++
