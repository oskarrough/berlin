+++
body = "Sandbox Interactive is an ambitious game development studio based in the heart of Berlin. Albion Online is the first true cross-platform MMO and is playable on Windows, Mac, Linux, iOS and Android. #games "
created_at = "2020-01-25T15:59:24.912Z"
is_approved = true
job_board_url = "https://albiononline.com/en/jobs"
latitude = 52.5429975
longitude = 13.4116673
slug = "sandbox-interactive"
tags = ["games"]
title = "Sandbox Interactive"

+++
