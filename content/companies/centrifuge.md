+++
body = "Centrifuge is the decentralized operating system to power global trade and commerce. We are building the systems and tools on public blockchain infrastructure to enable open, fair, and transparent business within the Financial Supply Chain. #blockchain #startup #tech"
created_at = "2020-01-06T14:33:38.000Z"
is_approved = true
job_board_url = "https://centrifuge.io/careers"
slug = "centrifuge"
tags = ["blockchain", "startup", "tech"]
title = "Centrifuge"

+++
