+++
body = "#school #teaching"
created_at = "2018-02-15T07:18:54.738Z"
is_approved = true
job_board_url = "http://www.berlin-international-school.de/index.php/en/job-opportunities-bis-en.html"
latitude = 52.4704836
longitude = 13.2935995
slug = "berlin-international-school"
tags = ["school", "teaching"]
title = "Berlin International School"
updated_at = "2019-06-16T10:36:09.635Z"

+++
