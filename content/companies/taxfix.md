+++
body = "#startup digital tax #accountant, for #tax declaration and returns"
created_at = "2018-02-02T10:17:11.963Z"
is_approved = true
job_board_url = "https://taxfix.de/jobs/"
latitude = 52.49879
longitude = 13.45873
slug = "taxfix"
tags = ["startup", "accountant", "tax"]
title = "Taxfix"
updated_at = "2019-06-16T10:36:08.518Z"

+++
