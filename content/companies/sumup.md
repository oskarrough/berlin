+++
body = "#startup #fintech building a device that allows small merchants to accept card payments anywhere with a smartphone"
created_at = "2018-02-01T19:59:03.064Z"
is_approved = true
job_board_url = "https://sumup.com/careers/positions/"
latitude = 52.5197018
longitude = 13.4127558
slug = "sumup"
tags = ["startup", "fintech"]
title = "SumUp"
updated_at = "2019-06-16T10:36:08.536Z"

+++
