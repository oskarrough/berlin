+++
body = "Edenspiekermann is #agency for #strategy, #design and #communication with offices in Amsterdam, Berlin, San Francisco, Los Angeles and Singapore"
created_at = "2017-06-28T20:35:40.962Z"
is_approved = true
job_board_url = "https://jobs.edenspiekermann.com/"
latitude = 52.5019054
longitude = 13.3654296
slug = "edenspiekermann"
tags = ["agency", "strategy", "design", "communication"]
title = "Edenspiekermann"
updated_at = "2019-06-16T10:36:08.518Z"

+++
