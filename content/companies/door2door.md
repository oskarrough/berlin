+++
body = "Door2door solution provides an economical and on-demand #transportation for citizens, employees or rural areas."
created_at = "2017-06-28T20:41:02.619Z"
is_approved = true
job_board_url = "https://www.door2door.io/jobs.html"
latitude = 52.5300431
longitude = 13.40297
slug = "door2door"
tags = ["transportation"]
title = "Door2door"
updated_at = "2019-06-16T10:36:09.626Z"

+++
