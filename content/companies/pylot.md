+++
job_board_url = "https://pylotjobs.recruitee.com/"
title = "Pylot"
slug = "pylot"
body = "Simplify the everyday life for entrepreneurs and their small and medium-sized companies with digital solutions. Our entrepreneur's cockpit bundles state of the art software solutions enabling owners to drive their business or to save time from manual processes and routines. With one of our first product business owners increase the overall online visibility of their company and foster customer proximity."
tags = ["startup", "software", "companies", "entrepreneur"]
address = "Hardenbergstraße 32"
postal_code = "10623"
city="berlin"
country="germany"
latitude = 52.508326
longitude = 13.329166
created_at = "2020-04-04T14:37:09.626Z"
updated_at = "2020-04-04T14:37:09.626Z"
company_url = "https://www.pylot.de/"
twitter_url = "https://twitter.com/wearepylot"
linkedin_url = "https://www.linkedin.com/company/pylot-de"
is_approved = true
draft = false
+++
