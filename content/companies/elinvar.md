+++
body = "Digital platform for banks and asset managers. #bank #finance"
created_at = "2018-01-14T18:35:36.022Z"
is_approved = true
job_board_url = "https://elinvar.recruitee.com/"
latitude = 52.53341
longitude = 13.4240201
slug = "elinvar"
tags = ["bank", "finance"]
title = "Elinvar"
updated_at = "2019-06-16T10:36:09.625Z"
+++
