+++
body = "CrateDB is an #open-source and distributed SQL #database management system."
created_at = "2017-06-28T20:41:10.602Z"
is_approved = true
job_board_url = "https://crate.io/jobs/"
latitude = 52.503673
longitude = 13.407628
slug = "crate"
tags = ["open-source", "database"]
title = "Crate"
updated_at = "2019-06-16T10:36:09.637Z"

+++
