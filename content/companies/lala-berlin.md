+++
body = "Online #fashion store and #agency; #ecommerce #startup"
created_at = "2018-02-14T07:29:11.441Z"
is_approved = true
job_board_url = "https://www.lalaberlin.com/jobs/"
latitude = 52.5345253
longitude = 13.3996178
slug = "lala-berlin"
tags = ["fashion", "agency", "ecommerce", "startup"]
title = "lala berlin"
updated_at = "2019-06-16T10:36:08.540Z"

+++
