+++
body = "Workspace as a Service. Flexible offices with the best price-performance and efficient sustainability. We offer everything from team offices to tailor-made offices and facilities for your meetings and events. #coworking spaces #startup from Berlin"
created_at = "2020-01-10T09:30:50.000Z"
is_approved = true
job_board_url = "https://unicornjobs.heavenhr.com/jobs"
latitude = 52.5419594
longitude = 13.3873236
slug = "unicorn"
tags = ["coworking", "startup"]
title = "Unicorn"
updated_at = "2019-06-16T10:46:08.000Z"

+++
