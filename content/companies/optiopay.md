+++
body = "OptioPay is a #payment solution software for #marketing money by processing payments. It allows users’ customers, employees, and partners to receive their payments with multiple payment options, including pre-paid cards from retailers."
created_at = "2017-06-28T20:33:42.998Z"
is_approved = true
job_board_url = "http://www.optiopay.com/jobs"
latitude = 52.4954784
longitude = 13.4619499
slug = "optiopay"
tags = ["payment", "marketing"]
title = "Optiopay"
updated_at = "2019-06-16T10:36:09.627Z"

+++
