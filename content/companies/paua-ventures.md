+++
job_board_url = "https://pauaventures1.recruitee.com"
slug = "paua-ventures"
title = "Paua Ventures"
body = "Paua Ventures is an early stage #venture-capital firm based in Berlin providing capital to #b2b #software companies."
latitude = 52.523591
longitude = 13.404883
tags = ["venture-capital", "b2b", "software"]
created_at = "2017-06-28T20:21:09.264Z"
updated_at = "2019-06-16T10:36:09.745Z"
company_url = "https://www.pauaventures.com"
linkedin_url = "https://www.linkedin.com/company/paua-ventures-gmbh"
twitter_url = "https://twitter.com/pauaventures"
is_approved = true
draft = false
+++
