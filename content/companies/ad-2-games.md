+++
body = "Creates #digital #marketing strategies with a focus on both free-to-play and pay-to-play #games for PC and consoles"
created_at = "2017-06-28T20:29:18.037Z"
is_approved = true
job_board_url = "https://www.ad2games.com/jobs"
latitude = 52.507636
longitude = 13.4144283
slug = "ad-2-games"
tags = ["digital", "marketing", "games"]
title = "Ad 2 Games"
updated_at = "2019-06-16T10:36:09.730Z"

+++
