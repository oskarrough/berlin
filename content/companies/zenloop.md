+++
job_board_url = "https://zenloopgmbh.recruitee.com"
title = "zenloop"
slug = "Zenloop"
body = "Net Promoter Score (NPS) Feedback Management. Customer retention on software for e-commerce platforms and lead-focused companies"
tags = ["startup", "software", "customer-retention"]
address = "Erich-Weinert-Str. 145"
postal_code = "10409"
city="berlin"
country="germany"
latitude = 52.544114
longitude = 13.440539
created_at = "2020-04-04T15:48:08.626Z"
updated_at = "2020-04-04T15:48:08.626Z"
company_url = "https://www.zenloop.com"
linkedin_url = "https://www.linkedin.com/company/zenloop"
twitter_url = "https://twitter.com/zenloop_com"
facebook_url = "https://www.facebook.com/zenloop"
googleMapsUrl = "https://g.page/zenloop-gmbh"
crunchBaseUrl = "https://www.crunchbase.com/organization/zenloop"
is_approved = true
draft = false
+++
