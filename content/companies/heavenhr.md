+++
body = "#hr management platform. #software #startup #tech"
created_at = "2017-08-05T13:20:55.147Z"
is_approved = true
job_board_url = "https://www.heavenhr.com/DE/de/jobs"
latitude = 52.4940644
longitude = 13.3969221
slug = "heavenhr"
tags = ["hr", "software", "startup", "tech"]
title = "HeavenHR"
updated_at = "2019-06-16T10:36:09.625Z"

+++
