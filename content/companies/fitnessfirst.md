+++
body = "Fitness First is the largest privately owned health club group in the world. It consists of more than 360 Fitness First clubs worldwide. #gym #fitness #sport"
created_at = "2018-02-09T15:06:12.336Z"
is_approved = true
job_board_url = "https://www.fitnessfirst.de/karriere/jobs?text=&type=All&location=959"
slug = "fitnessfirst"
tags = ["gym", "fitness", "sport"]
title = "FitnessFirst"
updated_at = "2019-06-16T10:36:08.529Z"

+++
