+++
body = "Online payments system provider, that supports online money transfers and serves as an electronic alternative to traditional paper methods like checks and money orders. #technology #fintech #bank"
created_at = "2018-02-06T11:49:51.369Z"
is_approved = true
job_board_url = "https://jobsearch.paypal-corp.com/en-US/search?keywords=&location=berlin&facetcountry=de"
latitude = 52.4465324
longitude = 13.2576329
slug = "paypal"
tags = ["technology", "fintech", "bank"]
title = "PayPal"
updated_at = "2019-06-16T10:36:08.536Z"

+++
