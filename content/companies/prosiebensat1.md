+++
body = "ProSiebenSat.1 Group is one of Europe's leading media companies. TV entertainment, leading broadcasting, digital entertainment, and commerce powerhouse. #media #tv"
created_at = "2018-02-10T15:09:43.597Z"
is_approved = true
job_board_url = "https://www.prosiebensat1-jobs.com/stellenangebote.html?reset_search=0&search_mode=job_filter_advanced&filter%5Bvolltext%5D=&filter%5Bcountr%5D%5B%5D=41"
slug = "prosiebensat1"
tags = ["media", "tv"]
title = "ProSiebenSat.1"
updated_at = "2019-06-16T10:36:09.631Z"

+++
