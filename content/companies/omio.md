+++
body = "#travel search engine comparing trains, buses flights. #startup #tech"
created_at = "2017-06-28T20:18:56.204Z"
is_approved = true
company_url = "https://omio.com"
job_board_url = "https://www.omio.com/jobs"
job_board_provider = "smartrecruiters"
job_board_hostname = "Omio1"
latitude = 52.5308501
longitude = 13.4109222
slug = "omio"
tags = ["travel", "startup", "tech"]
title = "Omio"
updated_at = "2019-06-16T10:36:09.730Z"
+++
