+++
body = "Adjust has developed a #mobile solution tunify all #marketing activities into one #platform, giving the insights you need to scale your bus."
created_at = "2017-06-28T20:36:43.172Z"
is_approved = true
job_board_url = "https://adjust.bamboohr.co.uk/jobs/"
latitude = 52.5283174
longitude = 13.4152739
slug = "adjust"
tags = ["mobile", "marketing", "platform"]
title = "Adjust"
updated_at = "2019-06-16T10:36:09.623Z"

+++
