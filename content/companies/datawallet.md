+++
body = "Datawallet is harnessing the power of the #blockchain to finally give people ownership of their data and make it work for them. #startup"
created_at = "2019-11-15T18:10:42.138Z"
is_approved = true
job_board_url = "https://angel.co/company/datawallet/jobs"
latitude = 52.5322441
longitude = 13.4079344
slug = "datawallet"
tags = ["blockchain", "startup"]
title = "Datawallet"

+++
