+++
body = "#classical-music streaming service. #music #startup #technology"
created_at = "2018-02-06T17:26:06.765Z"
is_approved = true
job_board_url = "https://idagio-jobs.personio.de"
company_url = "https://idagio.com"
latitude = 52.4985508
longitude = 13.383492
slug = "idagio"
tags = ["classical-music", "music", "startup", "technology"]
title = "Idagio"
updated_at = "2019-06-16T10:36:08.540Z"

+++
