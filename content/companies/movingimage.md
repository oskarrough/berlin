+++
body = "movingimage is a global leader in delivering secure enterprise video solutions. Simply put, we want to revolutionize the way in which companies use video. Over 500 enterprises including DAX-listed corporations such as the Volkswagen Group and Deutsche Telekom, already use our secure Enterprise Video Platform (EVP). \n#video #b2b"
created_at = "2018-02-07T20:55:50.637Z"
is_approved = true
job_board_url = "https://www.movingimage.com/career/"
latitude = 52.50002
longitude = 13.454663
slug = "movingimage"
tags = ["video", "b2b"]
title = "movingimage"
updated_at = "2019-06-16T10:36:08.528Z"

+++
