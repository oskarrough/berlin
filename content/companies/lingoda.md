+++
job_board_url = "https://lingoda.recruitee.com"
title = "Lingoda"
slug = "lingoda"
body = "Lingoda offers online language lessons with qualified native speaking teachers."
tags = ["languages", "teaching", "online-classes"]
address = "Zimmerstraße 69"
postal_code = "10117"
city="berlin"
country="germany"
latitude = 52.508087 
longitude = 13.392802 
created_at = "2020-04-04T15:48:08.626Z"
updated_at = "2020-04-04T15:48:08.626Z"
company_url = "lingoda.com"
googleMapsUrl = "https://g.page/lingoda_official"
linkedin_url = "https://www.linkedin.com/company/lingoda-gmbh"
twitter_url = "https://twitter.com/lingoda"
is_approved = true
draft = false
+++
