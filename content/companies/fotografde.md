+++
body = "Sales platform for photographers. #startup #photography"
created_at = "2018-06-21T18:35:47.528Z"
is_approved = true
job_board_url = "https://www.fotograf.de/jobs/#jobs"
latitude = 52.5329113
longitude = 13.4281757
slug = "fotografde"
tags = ["startup", "photography"]
title = "Fotograf.de"
updated_at = "2019-06-16T10:36:08.537Z"

+++
