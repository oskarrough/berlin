+++
body = "AppLift offers a #solution to empower businesses to connect with and activate customers on #mobile"
created_at = "2017-06-28T20:29:29.930Z"
is_approved = true
job_board_url = "http://www.applift.com/open-positions.html"
latitude = 52.521412
longitude = 13.405186
slug = "applift"
tags = ["solution", "mobile"]
title = "AppLift"
updated_at = "2019-06-16T10:36:09.737Z"

+++
