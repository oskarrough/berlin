+++
body = "Boiler Room is a global online #music #broadcasting #platform commissioning and #streaming live music sessions around the world"
created_at = "2017-06-28T20:35:57.019Z"
is_approved = true
job_board_url = "https://boilerroom.tv/jobs/"
slug = "boilerroom"
tags = ["music", "broadcasting", "platform", "streaming"]
title = "Boilerroom"
updated_at = "2019-06-16T10:36:09.730Z"

+++
