+++
title = "Info"
+++

## About

<!-- **[joblist.city](https://joblist.city)** is a collection of projects focusing on *work*. -->

**Job List Berlin** is an open website for job offers and companies specificaly hiring in the city of [Berlin](https://en.wikipedia.org/wiki/Berlin), Germany (Europe).

Feel free to add missing or update existing information.


## Publish a job

There is the possibility to create and publish job offers on this site.

### Manual publishing

All *jobs* which have the "hashtag" **#berlin**, are shown on the homepage of [joblistberlin.com](https://joblistberlin.com).

Similarely, if you want a job to appear on any #tag page, add this #tag to the job's description.

Example new Job:

> **Title** Gardener @ my-company
> **Url** https://example.com/gardener-job-at-permaculture-project.html
> **Description** Join our team if you like #permaculture want to plan
> #trees, associate #plants, and develop #software for the #nature.


### Automatic publishing

If a company is listed on joblist-berlin, it is possible to automatically list all its available job offers.

For this to work, the company needs to be using a known "job board provider", from which the API can be called to retrieve jobs.

Look at the [list of known providers](https://gitlab.com/joblist/berlin/-/blob/master/themes/link-list/static/js/components/list/jobs.js#L16), [how to implement a new provider](https://gitlab.com/joblist/berlin/-/tree/master/themes/link-list/static/js/components/providers), and [how to describe a provider on a company file](https://gitlab.com/joblist/berlin/-/blob/master/content/companies/omio.md).

## Contact

Don't hesitate to get in touch:

- [#joblistcity:matrix.org](https://riot.im/app/#/room/#joblistcity:matrix.org)
- [Email](mailto:info@joblist.city)
- [Facebook](https://www.facebook.com/joblistberlin)
- [Linkedin](https://www.linkedin.com/company/joblistberlin)
- [gitlab/joblist/berlin](https://gitlab.com/joblist/berlin)

## Terms of & list of services

Please, read and agree with the following licenses and terms for our
systems and softwares, and the one they depend on.

Internal:
- code license: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) or [aGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html).
- data license: [odbl](https://opendatacommons.org/licenses/odbl/)
- content license: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
- content & code & data attributions: [JobListCity contributors](https://joblist.city)

Dependencies:
- [OSM](https://www.openstreetmap.org/copyright) (Open Street Map)
- [Firebase](https://firebase.google.com/terms/)
- [Algolia](https://www.algolia.com/policies/terms/)
