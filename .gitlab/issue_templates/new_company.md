To create a new company, replace the information in the template bellow to describre the company you would like to submit.

Required (don't worry if you can't fill everything):
```
+++
jobBoardUrl = "https://www.quinoa-bildung.de/organisation/jobs"
title = "Quinoa Bildung"
body = "School and youth formations"
tags = ["school", "teaching"]
address = "Kühnemannstrasse 26"
postalCode = "12033"
city="berlin"
country="germany"
companyUrl = "https://www.quinoa-bildung.de/"
+++
```

Optional:
```
slug = "quinoa-bildung"
jobBoardProvider = "smartrecruiters"
jobBoardHostname = "QuinoaBildungGGmbH"
latitude = 52.566119
longitude = 13.381054
googleMapsUrl = "https://g.page/the-student-hotel-berlin"
linkedinUrl = "https://linkedin.com/quinoabildung"
facebookUrl = "https://www.facebook.com/quinoabildung"
instagramUrl = "https://www.instagram.com/quinoabildung"
createdAt = "2020-07-04T23:10:18.626Z"
updatedAt = "2020-07-04T23:10:18.626Z"
isApproved = true
draft = false
```
